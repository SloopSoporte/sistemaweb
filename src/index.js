import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import SignUp from './SignUp'
import Show from './Show'
import Edit from './Edit'
import login from './login'
import report from './report'
import { BrowserRouter as Router, Route } from 'react-router-dom';
import EditPassword from './EditPassword';

ReactDOM.render(
    //rutas
    <Router>
        <div>
            <Route exact path='/' component={login}/>
            <Route path='/Home' component={App}/>
            <Route path='/create' component={SignUp}/>
            <Route path='/show/:id' component={Show}/>
            <Route path='/edit/:id' component={Edit}/>
            <Route path='/editP/:id' component={EditPassword}/>
            <Route path='/report/' component={report}/>
        </div>
    </Router>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
