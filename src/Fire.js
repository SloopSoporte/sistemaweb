import firebase from 'firebase';

const config = { /* COPY THE ACTUAL CONFIG FROM FIREBASE CONSOLE */
    //firebase conexion
    apiKey: "AIzaSyCqbQm_Rvmgva-6atXmAagFPE6dyh-CKBE",
    authDomain: "sloop-dev.firebaseapp.com",
    databaseURL: "https://sloop-dev.firebaseio.com",
    projectId: "sloop-dev",
    storageBucket: "sloop-dev.appspot.com",
    messagingSenderId: "784544137746"
};
const fire = firebase.initializeApp(config);
export default fire;