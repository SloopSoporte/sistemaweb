import React, { Component } from 'react'
import fire from './Fire';
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router-dom'

class Show extends Component{
// muestra doctores
    constructor(props){
        super(props);
        const token = localStorage.getItem("userToken")
        let loggedIn = true
        if (token == null){
            loggedIn = false
        }
        this.state = {
            doctors: {},
            key: '',
            loggedIn
        };
    }

    componentDidMount(){
        const ref = fire.firestore().collection('doctors').doc(this.props.match.params.id);
        ref.get().then((doc) => {
            if(doc.exists) {
                this.setState({
                    doctors: doc.data(),
                    key: doc.id,
                    isLoading: false
                });
            }else {
                console.log("No existe el usuario");
            }
        });
    }

    delete(id){
        var user = fire.auth().currentUser;

        user.delete().then(function() {
        // User deleted.
        }).catch(function(error) {
        // An error happened.
        });
        fire.firestore().collection('doctors').doc(id).delete().then(() => {
            console.log("se ha eliminado al usuario");
            this.props.history.push("/")
        }).catch((error) => {
            console.error("No se pudo eliminar al usuario: ", error);
        });
        alert('se elimino al usuario correctamente.')
    }
    login(email, password){
        fire.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // ...
          });
          var user = fire.auth().currentUser;
          if(user!= null){
            alert('esta logeado el usuario ');
          }
          console.log(user);
    }
    Logout(){
        var user = fire.auth().currentUser;
        fire.auth().signOut().then(function() {
            // Sign-out successful.
        }).catch(function(error) {
            // An error happened.
        });
        if(user == null){
            alert('se ha deslogeado el usuario');
        }
    }
    check(){
        var user = fire.auth().currentUser;
        if(user != null){
            alert('esta logeado el usuario');
        }else{
            alert('el usuario no esta logeado');
        }

    }
    logOut(e){
        e.preventDefault()
        localStorage.removeItem('userToken')
        this.props.history.push('/')
      }
    render(){
        if(this.state.loggedIn === false){
            return <Redirect to="/"/>
        }
        return(
            <div className="container">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <h1 class="navbar-brand" >Sloop</h1>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" ><Link to="/">Doctores</Link></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" ><Link to="/create">Agregar Doctor</Link></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link"><Link to="/report">Reporte</Link></a>
                    </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0" >
                    <button onClick={this.logOut.bind(this)} class="btn btn-outline-danger my-2 my-sm-0" type="submit">Logout</button>
                    </form>
                </div>
            </nav>
            <br/>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <br/>
                        <button className="btn btn-warning"><Link to="/">Lista Doctores</Link></button>
                        <br/>
                        <br/>
                        <p>El usuario debe estar logeado para poder ser editado o eliminado.</p>
                        <h3 className="panel-title">
                            {this.state.doctors.nombres}
                        </h3>
                    </div>
                    <div className="panel-body">
                        <dl>
                            <dt>Nombres:</dt>
                            <dd>{this.state.doctors.nombres}</dd>
                            <dt>Apellidos:</dt>
                            <dd>{this.state.doctors.apellidos}</dd>
                            <dt>Celular:</dt>
                            <dd>{this.state.doctors.telefono}</dd>
                            <dt>Licencia Medica:</dt>
                            <dd>{this.state.doctors.licenciaMedica}</dd>
                            <dt>Licencia Conducir:</dt>
                            <dd>{this.state.doctors.licenciaConducir}</dd>
                            <dt>CURP:</dt>
                            <dd>{this.state.doctors.CURP}</dd>
                            <dt>Especialidad:</dt>
                            <dd>{this.state.doctors.especialidad}</dd>
                            <dt>Email:</dt>
                            <dd id="email">{this.state.doctors.email}</dd>
                            <dt>Password</dt>
                            <dd type="password" id="password">{this.state.doctors.password}</dd>
                        </dl>
                        <Link to={`/edit/${this.state.key}`} className="btn btn-success">Editar</Link>&nbsp;
                        <Link to={`/editP/${this.state.key}`} className="btn btn-success">Editar Contrasena</Link>&nbsp;
                        <button onClick={this.delete.bind(this, this.state.key)} className="btn btn-light"><Link to="/">Eliminar</Link></button>
                        <button onClick={this.login.bind(this, this.state.doctors.email, this.state.doctors.password)} className="btn btn-info">Login</button>
                        <button onClick={this.Logout} className="btn btn-info">Logout</button>
                        <button onClick={this.check} className="btn btn-light">check</button>
                    </div>
                </div>
            </div>
        );
    }

}

export default Show;