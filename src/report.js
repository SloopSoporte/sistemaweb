import React, { Component } from 'react';
import './App.css';
import { Link } from 'react-router-dom';
import fire from './Fire';
import Home from './Home';
import SignUp from './SignUp';
import { Redirect } from 'react-router-dom';
import jwt_decode from 'jwt-decode'
import { IoIosPersonAdd,  IoIosTimer } from 'react-icons/io';
import {  FaMoneyBill, FaBusinessTime, FaTimesCircle, FaTimes } from 'react-icons/fa';
import jsPDF from 'jspdf';
import 'jspdf-autotable';

const doc = new jsPDF();
doc.autoTable({ html: '#my-table' });

class report extends Component {

 //reportes

  constructor(props) {
    super(props);
    const token = localStorage.getItem("userToken")
    let loggedIn = true
    if (token == null) {
      loggedIn = false
    }
    this.ref = fire.firestore().collection('rides');
    this.unsubscribe = null;
    this.state = {
      rides: [],
      loggedIn
    };
  }

  print() {
    doc.autoTable({
      head: [
        ['Datos del conductor', 'Viajes']
      ]
    });
    this.state.rides.map(rides =>
      doc.autoTable({
        body: [
          [rides.DateTime, rides.DateTime]
        ]
      })
    )
    doc.save('Reporte.pdf')
  }

  onCollectionUpdate = (querySnapshots) => {
    const rides = [];
    querySnapshots.forEach((doct) => {
      const { DateTime, lat, lng, rideStatus, GeoPlaces, nombre } = doct.data();
      rides.push({
        key: doct.id,
        doct,
        DateTime,
        lat,
        lng,
        rideStatus,
        GeoPlaces,
        nombre
      });
    });
    this.setState({
      rides
    });
  }

  componentDidMount() {
    const token = localStorage.usertoken
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }

  logOut(e) {
    e.preventDefault()
    localStorage.removeItem('userToken')
    this.props.history.push('/')
  }
  render() {


    if (this.state.loggedIn === false) {
      return <Redirect to="/" />
    }
    return (

      <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <h1 class="navbar-brand" >Sloop</h1>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" ><Link to="/">Doctores</Link></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" ><Link to="/create">Agregar Doctor</Link></a>
              </li>
              <li class="nav-item">
                <a class="nav-link"><Link to="/report">Reporte</Link></a>
              </li>
            </ul>
            <form class="form-inline my-2 my-lg-0" >
              <button onClick={this.logOut.bind(this)} class="btn btn-outline-danger my-2 my-sm-0" type="submit">Logout</button>
            </form>
          </div>
        </nav>

        <br />
        <div class="plan panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Reporte
          </h3>



            <div class="card-body d-flex justify-content-between align-items-center"></div>
            <button
              class="btn btn-warning"
              onClick={() => this.print()} 

           >
            Vista previa
            </button>




          </div>
          <div class="panel-body">
            <table className="table table-stripe">
              <thead>
                <tr>
                  <th>Datos del conductor</th>
                  <th>Viajes</th>
                </tr>
              </thead>
              <tbody>

                {this.state.rides.map(rides =>
                  <tr>
                    <td>
                      <div class="row"><span class="glyphicon glyphicon-triangle-right"></span>
                        <IoIosPersonAdd />
                        <div class=".col-xs-2 .col-md-6">  {rides.nombre}</div>

                      </div>
                      <div class="row">

                        <div class=".col-xs-2 .col-md-6">  {rides.GeoPlaces}</div>
                      </div>
                      <div class="row">

                        <div class=".col-xs-2 .col-md-6">  {rides.lat}</div>
                      </div>
                    </td>

                    <td>
                      <div class="row">


                        <div class="col-sm-8" ><IoIosTimer /> {rides.lng}</div>

                        <div class="col-sm-8" >< FaMoneyBill /> {rides.lat}</div>

                      </div>
                    </td>

                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}

export default report;