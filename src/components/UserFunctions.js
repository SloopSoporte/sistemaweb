import axios from 'axios'

export const register = newUser => {
    //dar de alta clientes.
    return axios.post('users/register', {
        email: newUser.email,
        password: newUser.password
    })
    .then(res => {
        console.log("registrado")
    })
}

export const logear = user => {
    //accion de login
    return axios.post('users/login', {
        email: user.email,
        password: user.password
    })
    .then(res => {
        localStorage.setItem('userToken',res.data)
        return res.data
    })
    .catch(err => {
        console.log(err)
    })
}