import React, { Component } from 'react';
import './App.css';
import { Link } from 'react-router-dom';
import fire from './Fire';
import Home from './Home';
import SignUp from './SignUp';
import { Redirect } from 'react-router-dom'
import jwt_decode from 'jwt-decode'

class App extends Component {
  //lista de doctores
  constructor(props){
    super(props);
    const token = localStorage.getItem("userToken")
    let loggedIn = true
    if (token == null){
      loggedIn = false
    }
    this.ref = fire.firestore().collection('doctors');
    this.unsubscribe = null;
    this.state = {
      doctors: [],
      loggedIn
    };
  }

  onCollectionUpdate = (querySnapshots) => {
    const doctors = [];
    querySnapshots.forEach((doc) => {
      const { apellidos, nombres, email, licenciaMedica,CURP } = doc.data();
      doctors.push({
        key: doc.id,
        doc,
        apellidos,
        nombres,
        email,
        licenciaMedica,
        CURP
      });
    });
    this.setState({
      doctors
    });
  }

  componentDidMount(){
    const token = localStorage.usertoken
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }

  logOut(e){
    e.preventDefault()
    localStorage.removeItem('userToken')
    this.props.history.push('/')
  }
  render() {
    if(this.state.loggedIn === false){
      return <Redirect to="/"/>
    }
    return (
      <div class="container">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <h1 class="navbar-brand" >Sloop</h1>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" ><Link to="/">Doctores</Link></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" ><Link to="/create">Agregar Doctor</Link></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"><Link to="/report">Reporte</Link></a>
                    </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0" >
                    <button onClick={this.logOut.bind(this)} class="btn btn-outline-danger my-2 my-sm-0" type="submit">Logout</button>
                    </form>
                </div>
            </nav>
            <br/>
      <div class="plan panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">
            Lista de Doctores.
          </h3>
        </div>
        <div class="panel-body">
          <table className="table table-stripe">
            <thead>
              <tr>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Email</th>
                <th>Licencia Medica</th>
                <th>CURP</th>
              </tr>
            </thead>
            <tbody>
              {this.state.doctors.map(doctors => 
                <tr>
                  <td>{doctors.nombres}</td>
                  <td>{doctors.apellidos}</td>
                  <td>{doctors.email}</td>
                  <td>{doctors.licenciaMedica}</td>
                  <td>{doctors.CURP}</td>
                  <button className="btn btn-light"><Link  to={`/show/${doctors.key}`}>Mostrar</Link></button>
                </tr>
              )}
            </tbody>
          </table>
        </div>
       </div>
    </div>
    )}
}

 export default App;