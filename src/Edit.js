import React, { Component } from 'react'
import fire from './Fire';
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router-dom'

class Edit extends Component {
    //editar doctores
    constructor(props) {
        super(props);
        const token = localStorage.getItem("userToken")
        let loggedIn = true
        if (token == null){
            loggedIn = false
        }
        this.state = {
            key: '',
            apellidos:'',
            nombres:'',
            password:'',
            email:'',
            telefono:'',
            licenciaMedica:'',
            licenciaConducir:'',
            CURP:'',
            especialidad:'',
            loggedIn
        };
    }
    
    componentDidMount(){
        const ref = fire.firestore().collection('doctors').doc(this.props.match.params.id);
        ref.get().then((doc) => {
            if (doc.exists) {
                const doctors = doc.data();
                this.setState({
                    key: doc.id,
                    apellidos: doctors.apellidos,
                    nombres: doctors.nombres,
                    password: doctors.password,
                    email: doctors.email,
                    telefono: doctors.telefono,
                    licenciaMedica: doctors.licenciaMedica,
                    licenciaConducir:doctors.licenciaConducir,
                    CURP:doctors.CURP,
                    especialidad: doctors.especialidad
                });
            } else{
                console.log("No existe el usuario!");
            }
        });
    }

    onChange = (e) => {
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState({doctors:state});
    }

    validateForm(){

        let fields = this.state;
        let formIsValid = true;
    
        if (!fields["nombres"]) {
          formIsValid = false;
        }
    
        if (typeof fields["nombres"] !== "undefined") {
          if (!fields["nombres"].match(/^[a-zA-Z ]*$/)) {
            formIsValid = false;
          }
        }
    
        if (!fields["apellidos"]) {
          formIsValid = false;
        }
    
        if (typeof fields["apellidos"] !== "undefined") {
          if (!fields["apellidos"].match(/^[a-zA-Z ]*$/)) {
            formIsValid = false;
          }
        }
    
        if (!fields["CURP"]) {
          formIsValid = false;
        }
    
        if (typeof fields["CURP"] !== "undefined") {
          if (!fields["CURP"].match(/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/)) {
            formIsValid = false;
          }
        }
    
        if (!fields["email"]) {
          formIsValid = false;
        }
    
        if (typeof fields["email"] !== "undefined") {
          //regular expression for email validation
          var pattern = new RegExp(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/i);
          if (!pattern.test(fields["email"])) {
            formIsValid = false;
          }
        }
    
        if (!fields["telefono"]) {
          formIsValid = false;
        }
    
        if (typeof fields["telefono"] !== "undefined") {
          if (!fields["telefono"].match(/^[0-9]{10}$/)) {
            formIsValid = false;
          }
        }
    
        if (!fields["password"]) {
          formIsValid = false;
        }
    
        if (typeof fields["password"] !== "undefined") {
          if (!fields["password"].match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}$/)) {
            formIsValid = false;
          }
        }
    
        return formIsValid;
    
    
    }

    onSubmit = (e) => {

        e.preventDefault();

        const { apellidos, nombres, password, telefono, email,lattitude, longitude,licenciaConducir, licenciaMedica,CURP,especialidad } = this.state;
        var user = fire.auth().currentUser;
      
        user.updateEmail(email).then(function() {
          // Update successful.
        }).catch(function(error) {
          // An error happened.
        });
        const updateRef = fire.firestore().collection('doctors').doc(this.state.key);
        updateRef.set({
            apellidos, 
            nombres, 
            password,
            telefono,
            email,
            lattitude:'0',
            longitude:'0',
            licenciaConducir,
            licenciaMedica,
            CURP,
            especialidad,
        }).catch((error) => {
            console.error("Error al cargar al Usuario: ",error);
        });
        alert('se guardaron los cambios exitosamente.');
    }
    logOut(e){
      e.preventDefault()
      localStorage.removeItem('userToken')
      this.props.history.push('/')
    }
    render(){
      if(this.state.loggedIn === false){
        return <Redirect to="/"/>
      }
        return(
            <div className="container">
              <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                  <h1 class="navbar-brand" >Sloop</h1>
                  <div class="collapse navbar-collapse" id="navbarNav">
                      <ul class="navbar-nav">
                      <li class="nav-item">
                          <a class="nav-link" ><Link to="/">Doctores</Link></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" ><Link to="/create">Agregar Doctor</Link></a>
                    </li>
                      <li class="nav-item">
                      <a class="nav-link"><Link to="/report">Reporte</Link></a>
                      </li>
                      </ul>
                      <form class="form-inline my-2 my-lg-0" >
                      <button onClick={this.logOut.bind(this)} class="btn btn-outline-danger my-2 my-sm-0" type="submit">Logout</button>
                      </form>
                  </div>
              </nav>
              <br/>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">
                            Editar Usuario
                        </h3>
                    </div>
                    <div className="panel-body">
                        <h4><Link to={`/show/${this.state.key}`} className="btn btn-primary">Regresar</Link></h4>
                        <ul>
                            <li>Todos los campos son requeridos</li>
                            <li>Todos los campos deben llenarse con informacion real</li>
                            <li>No se podra registrar ningun usuario si no esta bien un campo</li>
                            <li>La contrasena debe contener una mayuscula, una miniscula y un minimo de 8 caracteres</li>
                        </ul>
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label for="nombres">Nombre:</label>
                                <input type="text" className="form-control" name="nombres" value={this.state.nombres}
                                onChange={this.onChange} placeholder="Nombres" id="nombres"/>
                            </div>
                            <div className="form-group">
                                <label for="apellidos">Apellidos:</label>
                                <input type="text" className="form-control" name="apellidos" value={this.state.apellidos}
                                onChange={this.onChange} placeholder="Apellidos" id="apellidos"/>
                            </div>
                            <div className="form-group">
                                <label for="telefono">Numero de Celular:</label>
                                <input type="text" className="form-control" name="telefono" value={this.state.telefono}
                                onChange={this.onChange} placeholder="telefono" id="telefono"/>
                            </div>
                            <div className="form-group">
                                <label for="licenciaMedica">Licencia Medica:</label>
                                <input type="text" className="form-control" name="licenciaMedica" value={this.state.licenciaMedica}
                                onChange={this.onChange} placeholder="Licencia Medica" id="licenciaMedica"/>
                            </div>
                            <div className="form-group">
                                <label for="licenciaConducir">Licencia Conducir:</label>
                                <input type="text" className="form-control" name="licenciaConducir" value={this.state.licenciaConducir}
                                onChange={this.onChange} placeholder="Licencia Conducir" id="licenciaConducir"/>
                            </div>
                            <div className="form-group">
                                <label for="CURP">CURP:</label>
                                <input type="text" className="form-control" name="CURP" value={this.state.CURP}
                                onChange={this.onChange} placeholder="CURP" style={{textTransform: "uppercase"}} id="CURP"/>
                            </div>
                            <div className="form-group">
                                <label for="especialidad">especialidad:</label>
                                <input type="text" className="form-control" name="especialidad" value={this.state.especialidad}
                                onChange={this.onChange} placeholder="Especialidad" id="especialidad"/>
                            </div>
                            <div className="form-group">
                                <label for="email">email:</label>
                                <input type="email" className="form-control" name="email" value={this.state.email}
                                onChange={this.onChange} placeholder="Correo Electronico" id="email"/>
                            </div>
                            <div className="form-group">
                                <label for="password">Password:</label>
                                <input type="password" className="form-control" name="password" value={this.state.password}
                                onChange={this.onChange} placeholder="Password" id="password" disabled={true}/>
                            </div>
                            <button type="submit" className="btn btn-light" disabled={this.validateForm()!=true}> Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}

export default Edit;