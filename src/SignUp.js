import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import fire from './Fire';
import { Redirect } from 'react-router-dom'

class SignUp extends Component {
  constructor(props) {
    //registro
    super(props);
    const token = localStorage.getItem("userToken")
    let loggedIn = true
    if (token == null){
      loggedIn = false
    }
    this.handleChange = this.handleChange.bind(this);
    this.signup = this.signup.bind(this);
    this.state = {
      email: '',
      password: '',
      nombres:'',
      apellidos:'',
      telefono:'',
      lattitude:'',
      longitude:'',
      licenciaMedica:'',
      licenciaConducir:'',
      CURP:'',
      especialidad:'',
      loggedIn
    };
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }


  validateForm() {

    let fields = this.state;
    let formIsValid = true;

    if (!fields["nombres"]) {
      formIsValid = false;
    }

    if (typeof fields["nombres"] !== "undefined") {
      if (!fields["nombres"].match(/^[a-zA-Z ]*$/)) {
        formIsValid = false;
      }
    }

    if (!fields["apellidos"]) {
      formIsValid = false;
    }

    if (typeof fields["apellidos"] !== "undefined") {
      if (!fields["apellidos"].match(/^[a-zA-Z ]*$/)) {
        formIsValid = false;
      }
    }

    if (!fields["CURP"]) {
      formIsValid = false;
    }

    if (typeof fields["CURP"] !== "undefined") {
      if (!fields["CURP"].match(/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/)) {
        formIsValid = false;
      }
    }

    if (!fields["email"]) {
      formIsValid = false;
    }

    if (typeof fields["email"] !== "undefined") {
      //regular expression for email validation
      var pattern = new RegExp(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/i);
      if (!pattern.test(fields["email"])) {
        formIsValid = false;
      }
    }

    if (!fields["telefono"]) {
      formIsValid = false;
    }

    if (typeof fields["telefono"] !== "undefined") {
      if (!fields["telefono"].match(/^[0-9]{10}$/)) {
        formIsValid = false;
      }
    }

    if (!fields["password"]) {
      formIsValid = false;
    }

    if (typeof fields["password"] !== "undefined") {
      if (!fields["password"].match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}$/)) {
        formIsValid = false;
      }
    }

    return formIsValid;


  }


  signup(e){
    e.preventDefault();
    fire.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then((u)=>{
    }).then((u)=>{console.log(u)}).then(() => {
        //var uid = userData.uid; // The UID of recently created user on firebase
        var aux = fire.auth().currentUser;
        fire
          .firestore()
          .collection("doctors")
          .doc(aux.uid)
          .set({
            email: this.state.email,
            nombres: this.state.nombres,
            apellidos: this.state.apellidos,
            password: this.state.password,
            telefono: this.state.telefono,
            lattitude:'0',
            longitude:'0',
            licenciaMedica: this.state.licenciaMedica,
            licenciaConducir: this.state.licenciaConducir,
            CURP: this.state.CURP,
            especialidad: this.state.especialidad
          });
      })
    .catch((error) => {
        console.log(error);
      })
      alert('se ah creado el usuario exitosamente.')
  }
  logOut(e){
    e.preventDefault()
    localStorage.removeItem('userToken')
    this.props.history.push('/')
  }
  render() {
    if(this.state.loggedIn === false){
      return <Redirect to="/"/>
    }
    return (
        <div className="container">
          <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <h1 class="navbar-brand" >Sloop</h1>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" ><Link to="/">Doctores</Link></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" ><Link to="/create">Agregar Doctor</Link></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link"><Link to="/report">Reporte</Link></a>
                    </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0" >
                    <button onClick={this.logOut.bind(this)} class="btn btn-outline-danger my-2 my-sm-0" type="submit">Logout</button>
                    </form>
                </div>
            </nav>
            <br/>
          <form>
          <h4><Link to="/" class="btn btn-primary">Users list</Link></h4>
            <ul>
              <li>Todos los campos son requeridos</li>
              <li>Todos los campos deben llenarse con informacion real</li>
              <li>No se podra registrar ningun usuario si no esta bien un campo</li>
              <li>La contrasena debe contener una mayuscula, una miniscula y un minimo de 8 caracteres</li>
            </ul>
            <div class="form-group">
              <label for="nombres">Nombres:</label>
              <input type="text" class="form-control" name="nombres" value={this.state.nombres} onChange={this.handleChange} placeholder="Nombres" id="nombres"/>
            </div>
            <div class="form-group">
              <label for="apellidos">Apellidos:</label>
              <input type="text" class="form-control" name="apellidos" value={this.state.apellidos} onChange={this.handleChange} placeholder="Apellidos" id="apellidos"/>
            </div>
            <div class="form-group">
              <label for="telefono">Numero de Celular:</label>
              <input type="text" class="form-control" name="telefono" value={this.state.telefono} onChange={this.handleChange} placeholder="Telefono" id="telefono"/>
            </div>
            <div class="form-group">
              <label for="licenciaMedica">Licencia Medica:</label>
              <input type="text" class="form-control" name="licenciaMedica" value={this.state.licenciaMedica} onChange={this.handleChange} placeholder="Licencia Medica" id="licenciaMedica"/>
            </div>
            <div class="form-group">
              <label for="licenciaConducir">Licencia de Conducir:</label>
              <input type="text" class="form-control" name="licenciaConducir" value={this.state.licenciaConducir} onChange={this.handleChange} placeholder="Licencia de Conducir" id="licenciaConducir"/>
            </div>
            <div class="form-group">
              <label for="CURP">CURP:</label>
              <input type="text" class="form-control" name="CURP" value={this.state.CURP} onChange={this.handleChange} placeholder="CURP" id="CURP" style={{textTransform: "uppercase"}}/>
            </div>
            <div class="form-group">
              <label for="especialidad">Especialidad:</label>
              <input type="text" class="form-control" name="especialidad" value={this.state.especialidad} onChange={this.handleChange} placeholder="especialidad" id="especialidad"/>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input value={this.state.email} onChange={this.handleChange} type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input value={this.state.password} onChange={this.handleChange} type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" />
            </div>
            <button onClick={this.signup} style={{marginLeft: '25px'}} className="btn btn-success" disabled={this.validateForm()!=true}>Signup</button>
          </form>
 
        </div>
    );
  }

  
}

export default SignUp;