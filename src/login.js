import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { logear } from './components/UserFunctions'

class login extends Component{
    //login con SQLSERVER
    constructor(props){
        super(props);
        const token = localStorage.getItem("userToken")
        let loggedIn = true
        if (token == null){
            loggedIn = false
        }
        this.state = {
            email:'',
            password:'',
            loggedIn
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    
    onChange(e){
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    onSubmit(e){
        e.preventDefault();
        const user = {
            email: this.state.email,
            password: this.state.password
        }

        logear(user).then(res => {
            if(res) {
                this.setState({
                    loggedIn:true
                })
            }
        })

    }

    render(){
        if(this.state.loggedIn){
            return <Redirect to="/Home"/>
        }
        return(
            <div className="container">
                <div className="row">
                    <div className="col-md-6 mt-5 mx-auto">
                        <form noValidate onSubmit={this.onSubmit}>
                            <h1 className="h3 mb-3 font-weight-normal">Favor de logearse</h1>
                            <div className="form-group">
                                <label htmlFor="email">Correo Electronico</label>
                                <input type="email" className="form-control" name="email" placeholder="Ingrese Correo Electronico" value={this.state.email} onChange={this.onChange}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Contrasena</label>
                                <input type="password" className="form-control" name="password" placeholder="Ingrese contrasena" value={this.state.password} onChange={this.onChange}/>
                            </div>
                            <button type="submit" className="btn btn-lg btn-primary btn-block">Ingresar</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default login;